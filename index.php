<?php
include 'log/loging.php';
$TimeEx = ini_get('max_execution_time');
$TimeEx = $TimeEx / 30; //по 30 сек. на слово 
$TimeEx = round($TimeEx, 0, PHP_ROUND_HALF_DOWN);
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex"/>
    <meta name="robots" content="nofollow"/>
    <meta name="robots" content="noarchive"/>
    <title>Cловоформы</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
    <div class="navbar">Cловоформы v.1.0</div>

    <div class="container">

        <div id="text-1">
          <span>Исходный список слов:</span><br/>
          <textarea rows="10" cols="50" id="input" placeholder="Cлова через пробел или каждое слово с новой строки (максимальное количество слов: <?php echo $TimeEx;?>)"></textarea><br/>
          <button onclick="Send(input.value);">Отправить</button>
          <button onclick="ClearIn();">Очистить</button>          
        </div>

        <div id="text-2"> 
          <span>Результат:</span><br/> 
          <textarea rows="10" cols="50" id="output" readonly></textarea><br/>
          <button onclick="ClearOut();">Очистить</button>
        </div>  

    </div>

</body>
</html>
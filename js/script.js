function Send(data){
		var xmlhttp;
		try{
		    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e){
		try{
		    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E){
		   xmlhttp = false;
		}
		}
		if(!xmlhttp && typeof XMLHttpRequest!='undefined') {
		   xmlhttp = new XMLHttpRequest();
		}
		//--------------- IMG LOAD -------------------
		var img = document.createElement('img');
		img.src = 'image/load.gif';
		img.style.position = 'absolute';
		img.style.height = '150px';
		img.style.marginTop = '-162px';
		img.style.marginLeft = '-130px';

		//--------------- SEND -----------------------
		xmlhttp.open('POST', 'request.php', true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlhttp.onreadystatechange = function(){
			if (xmlhttp.readyState != 4) return;
			if(xmlhttp.status == 200){
				if(xmlhttp.responseText != ''){
					document.getElementById('output').value = xmlhttp.responseText;
					document.getElementById('text-2').removeChild(img);
				}else{
					console.log('Empty server response!');
				}
			}
		}
		xmlhttp.send('text='+data);
		document.getElementById('output').value = '';
		//--------------- ADD IMG LOAD -------------------
		document.getElementById('text-2').appendChild(img);
}

function ClearIn(){
	document.getElementById('input').value = '';
}

function ClearOut(){
	document.getElementById('output').value = '';
}
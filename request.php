<?php

$ALPH = array(
				1 => 'А',
				2 => 'Б',
				3 => 'В',
				4 => 'Г',
				5 => 'Д',
				6 => 'Е',
				7 => 'Ё',
				8 => 'Ж',
				9 => 'З',
				10 => 'И',
				11 => 'Й',
				12 => 'К',
				13 => 'Л',
				14 => 'М',
				15 => 'Н',
				16 => 'О',
				17 => 'П',
				18 => 'Р',
				19 => 'С',
				20 => 'Т',
				21 => 'У',
				22 => 'Ф',
				23 => 'Х',
				24 => 'Ц',
				25 => 'Ч',
				26 => 'Ш',
				27 => 'Щ',
				28 => 'Ы',
				29 => 'Э',
				30 => 'Ю',
				31 => 'Я'
			);

function Indexing($index)
{	
	global $ALPH;
	$fp = fopen('data/dict.opcorpora.txt', 'rb');
	rewind($fp);
	$x = 0;
	$lemma = 0;
	//начинаем построчно перебирать файл
	while(!feof($fp))
	{ 
		//получаем текущую позицию в файле для считывания
		$position = ftell($fp);
		//считывае строку
		$string = fgets($fp);
		//убираем пробелы и переносы
		$string = trim($string);
		//получаем номер леммы
		if(is_numeric($string)){
				$lemma = (int)$string;
				continue;	
		}
		//выделяем слово из записи(строки) для сравнения с буквой 
		preg_match('/([А-ЯЁ])+/u', $string, $words);
		//исключаем пустые строки
		if(isset($words[0]) && $words[0] != ''){
			//получаем позицию буквы и в то же время всей записи (формы слова)
			$pos = mb_strpos($words[0], $ALPH[$index]);
			if($pos === 0){
				//открываем файл с индексами для текущей буквы
				$fi = fopen('index/'.$index, 'ab');
				//формируем индекс
				$itpos = $lemma.'/'.$position.PHP_EOL;
				//пишем индекс в файл
				fputs($fi, $itpos, mb_strlen($itpos, 'UTF-8'));
				//сбрасывае буфер
				fflush($fi);
				//fclose($fi);
			}
		}
	}
	ftruncate($fi, filesize('index/'.$index)-1);
	fclose($fi);
	fclose($fp);
}
//-------------------------------------------------------------------------
function GetForms($ws)
{	
	global $ALPH;
	//инициализируем массив для результата
	$ITEM 	= array();
	$FORMS 	= array();
	//путь до директории с индексами
	$indPath = 'index';
	//обнуляем текущий индекс
	$index = 0;
	//увеличиваем регистр символов
	$ws = mb_strtoupper($ws, 'UTF-8');
	//берём первую букв в искомом слове
	$littera = mb_substr($ws, 0, 1);
	//получаем индекс из массива с алфавитом
	foreach ($ALPH as $key => $value) {
		if($value == $littera){
			$index = $key; 
		}
	}
	//проверяем
	if($index != 0){
		$fi = fopen($indPath.DIRECTORY_SEPARATOR.$index, 'rb');
		//Открываем словарь
		$fp = fopen('data/dict.opcorpora.txt', 'rb');
		//начинаем построчно перебирать файл
		while(!feof($fi))
		{ 
			//получаем строку
			$coord = fgets($fi);
			//убираем пробелы
			$coord = trim($coord);
			//разделяем строку на номер леммы и смещение 
			$LP = mb_split('\x2f', $coord);
			//переходим на смещение
			fseek($fp, $LP[1]);
			$string = fgets($fp);
			$string = trim($string);
			preg_match('/([А-ЯЁ-])+/u', $string, $words);
			if(isset($words[0]) && $words[0] != ''){
				if($words[0] == $ws){
					$ITEM[$LP[0]] = $string;
				}
			}				
		}
	}
	//ищем леммы с формами среди индексов
	foreach ($ITEM as $key => $value){
		rewind($fi);
		while(!feof($fi))
		{
			$index = fgets($fi);
			$LP = mb_split('\x2f', $index);
			if($LP[0] == $key)
			{
				fseek($fp, trim($LP[1]));
				$FORMS[] = fgets($fp);
			}	

		} 
	}
	fclose($fp);
	fclose($fi);
	//возвращаем все формы
	if(count($FORMS) > 0){
		$arr = false;
		//echo 'Нормальная форма: <br/>'.$ITEM[0][0].'<br>';
		foreach ($FORMS as $value) {
			$words 	= preg_split('/[\s\t]+/u', $value);
			//Убираем формы(слова) с одинаковым написанием
			//путем слиянии в массиве одинаковых ключей 
			$arr[$words[0]] = $value;
		}			
		return $arr;
	}else{
		return false;
	}	
}
$raw = strip_tags($_POST['text']);
$raw = trim($raw);
//Проверяем запрос на пустоту
if($raw == ''){ echo 'Ваш запрос пуст! Введите искомые слова.'; exit(); }
//убираем пробелы  запроса
$data = trim($raw);
//Убираем переносы строк, если есть.
$data = str_replace(PHP_EOL, " ", $data);
//делим запрос на слова
$arWORDS = explode(" ", $data);
//подсчитывае колличество слов
$TimeEx = ini_get('max_execution_time');
$TimeEx = $TimeEx / 30;
$CountTimeEx = round($TimeEx, 0, PHP_ROUND_HALF_DOWN);
if(count($arWORDS) > $CountTimeEx){ echo 'Превышен лимит на количество слов! Сократите количество.'; exit(); }
//перебираем 
foreach ($arWORDS as $word){
	//проверяем пустые слова, такое бывает если много пробелов межд словами
	if($word == ''){ continue; }
	if(preg_match('/[А-Яа-яЁё]+/u', $word) == 1){
		$arRes = GetForms($word); 		//поиск форм в файле(DB)
	}else{
		echo '------------------'.PHP_EOL;
		echo 'Cлово: "'.$word.'" - не соответствует формату!'.PHP_EOL;
		echo '------------------'.PHP_EOL;
		exit();
	}
	//проверяем результат
	if($arRes != false)
	{	
		//перебираем массив и выводим элементы в нижнем регистре
		foreach ($arRes as $key => $value) {
			echo mb_strtolower($key).PHP_EOL;
		}
	}else{
		echo 'По слову: "'.$word.'" - ничего не найдено!'.PHP_EOL;
	}
	if(count($arWORDS) > 1){ echo '------------------'.PHP_EOL; }
}
